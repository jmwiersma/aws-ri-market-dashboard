#!/bin/bash

echo "Getting list of Regions for full RI import"

all_regions=$(aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]' | sort)

while read -r region; do

echo "Checking AWS Region $region"
#this gets us all the raw data
aws ec2 describe-reserved-instances-offerings --include-marketplace --region $region > /home/ec2-user/instance/data/$region-raw.json

#this gets us only the Default RI
cat /home/ec2-user/instance/data/$region-raw.json | jq '[.ReservedInstancesOfferings[] | { OfferingType } + {AvailabilityZone} + {InstanceTenancy} + {ProductDescription} + {UsagePrice} + (.RecurringCharges[]) + {Marketplace} + {CurrencyCode} + {FixedPrice} + {Duration} + {ReservedInstancesOfferingId} + {InstanceType} ]' > /home/ec2-user/instance/data/$region-strip-ri.json
cat /home/ec2-user/instance/data/$region-strip-ri.json | jq -r '["OfferingType","AvailabilityZone","InstanceTenancy","Count","Price","ProductDescription","UsagePrice","Amount","Frequency","Marketplace","CurrencyCode","FixedPrice","Duration","ReservedInstancesOfferingId","InstanceType"] as $fields | $fields, (.[] | [.[$fields[]]]) | @csv'  > /home/ec2-user/instance/data/$region-ri.csv

#Now import the generic RI data in RI table MYSQL 
mysql -u<> -p<> --local_infile=1 awsri << EOF
LOAD DATA LOCAL INFILE '/home/ec2-user/instance/data/$region-ri.csv' 
INTO TABLE ri  
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\n' 
IGNORE 1 LINES 
(OfferingType, AvailabilityZone, InstanceTenancy, Count, Price, ProductDescription, UsagePrice, Amount, Frequency, Marketplace, CurrencyCode, FixedPrice, Duration, ReservedInstancesOfferingId, InstanceType);
EOF
echo "MYSQL import $region RI done"

done <<< "$all_regions"

#Opschonen input files
rm /home/ec2-user/instance/data/*

echo "All done"


