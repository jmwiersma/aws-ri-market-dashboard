#!/bin/bash

#Yes, this is a dirty export to get AWS CLI working in CRONTAB. Need to clean this
export AWS_CONFIG_FILE="/root/.aws/config"
export AWS_ACCESS_KEY_ID=<key>
export AWS_SECRET_ACCESS_KEY=<key>

echo "Clean up of old input files"
rm /home/ec2-user/instance/input/*

echo "Getting list of Regions for market import"

all_regions=$( /usr/bin/aws ec2 describe-regions --output text --query 'Regions[*].[RegionName]' | sort)

while read -r region; do

echo "Checking AWS Region $region"
#this gets us all the raw data
/usr/bin/aws ec2 describe-reserved-instances-offerings --include-marketplace --region $region > /home/ec2-user/instance/data/$region-raw.json

#this gets us only the marketplace
cat /home/ec2-user/instance/data/$region-raw.json | /usr/bin/jq '[.ReservedInstancesOfferings[] | { OfferingType } + {AvailabilityZone} + {InstanceTenancy} + (.PricingDetails[]) + {ProductDescription} + {UsagePrice} + (.RecurringCharges[]) + {Marketplace} + {CurrencyCode} + {FixedPrice} + {Duration} + {ReservedInstancesOfferingId} + {InstanceType} ]' > /home/ec2-user/instance/data/$region-strip.json
cat /home/ec2-user/instance/data/$region-strip.json | /usr/bin/jq -r '["OfferingType","AvailabilityZone","InstanceTenancy","Count","Price","ProductDescription","UsagePrice","Amount","Frequency","Marketplace","CurrencyCode","FixedPrice","Duration","ReservedInstancesOfferingId","InstanceType"] as $fields | $fields, (.[] | [.[$fields[]]]) | @csv'  > /home/ec2-user/instance/data/$region-market.csv

#strip headers
sed -i 1d /home/ec2-user/instance/data/$region-market.csv

date="$(date +%d-%m-%Y\ %H:%M)"
while read -r line; do
echo "$date,$line" >> /home/ec2-user/instance/input/instance-date-$region.csv
done < /home/ec2-user/instance/data/$region-market.csv


#Now import the marketplace in market table MYSQL as option , and capture warnings
#mysql -uXXX -pXXX --local_infile=1 awsri --show-warnings << EOF
#LOAD DATA LOCAL INFILE '/home/ec2-user/instance/data/$region-market.csv'
#INTO TABLE market
#FIELDS TERMINATED BY ','
#ENCLOSED BY '"'
#LINES TERMINATED BY '\n'
#IGNORE 1 LINES
#(OfferingType, AvailabilityZone, InstanceTenancy, Count, Price, ProductDescription, UsagePrice, Amount, Frequency, Marketplace, CurrencyCode, FixedPrice, Duration, ReservedInstancesOfferingId, InstanceType);
#EOF
#echo "MYSQL import $region market done"

done <<< "$all_regions"

#echo "Processing CSV with embulk"
#/home/ec2-user/.embulk/bin/embulk run /home/ec2-user/config.yml

#Clean input files
rm /home/ec2-user/instance/data/*

echo "All done"


